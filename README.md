# CI-CD-with-test-stage-beta



## Getting started

Flow of  build application in CI/CD pipeline  and deploy on different env `test` `staging` and `beta`

Following are stages:

stages:


`build`: build application and push to registry


`deploy_test`: deploy application on `test` env


`test_test`: test the application on `test` env


`deploy_staging`: deploy application on `staging` env


`test_staging`: test the application on `staging` env



`deploy_beta`: deploy application on `beta` env



Following flow shows in example:

1. commit on any branch => `build` , `deploy_test` , `test_test`
2. merge request create => `build`, `deploy_staging`, `test_staging`  (Ideally there is no build here)
3. merge request update => `build`, `deploy_staging`, `test_staging`
4. merge request merged => `deploy_beta`
